<?php

namespace App\Http\Controllers;

use App\Http\Requests\ADsRequest;
use App\Http\Requests\ImagesRequest;
use App\Models\AD;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use File;
class ADController extends Controller
{
    public function index(){
        $Ads = AD::all();
        return view('ADs.index')->with('Ads',$Ads);
    }

    public function create(){
        return view('ADs.create');
    }

    public function store(ADsRequest $request){
        $image = $request->file('image');
        $imageName = $image->getClientOriginalName();
        $destinationPath = public_path('\\upload');
        $img = Image::make($image->path());
        $img->resize(900,600)
            ->save($destinationPath.'\\'.$imageName);

        AD::create([
            'title'=> $request->title,
            'image'=>$imageName,
            'target_url'=>$request->target_url
        ]);
        return redirect(route('ADs.index'));
    }


    public function edit($AD){
        $Ads = AD::find($AD);
        return view('ADs.edit')->with('Ads',$Ads);
    }

    public function update(ADsRequest $request ,$AD){
        $Ads = AD::find($AD);
        $destinationPath = public_path('\\upload\\'.$Ads->image);
        if (File::exists($destinationPath)) File::delete($destinationPath);

        $image = $request->file('image');
        $imageName = $image->getClientOriginalName();
        $destinationPath = public_path('\\upload');
        $img = Image::make($image->path());
        $img->resize(900,600)
            ->save($destinationPath.'\\'.$imageName);

        $Ads->update([
           'title'=>$request->title,
           'image'=>$imageName,
           'target_url'=>$request->target_url
        ]);
        return redirect(route('ADs.index'));
    }

    public function destroy($AD){

        $Ads = AD::find($AD);
        if (File::exists(public_path('\\upload\\'.$Ads->image))) File::delete(public_path('\\upload\\'.$Ads->image));
        $Ads->delete();
        return redirect(route('ADs.index'));
    }
}
