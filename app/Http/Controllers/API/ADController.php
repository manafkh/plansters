<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ADResource;
use App\Models\AD;
use Illuminate\Http\Request;

class ADController extends Controller
{
    public function index(): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        $ADs = AD::all();
        return  ADResource::collection($ADs);
    }
}
