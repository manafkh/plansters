@extends('layouts.app')
@section('content')
    <table class="table table-dark">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Target URL</th>
            <th scope="col">Image</th>
            <th scope="col">ِActions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($Ads as $Ad)
        <tr>
            <th scope="row">{{$Ad->id}}</th>
            <td>{{$Ad->title}}</td>
            <td>{{$Ad->target_url}}</td>
            <td>{{$Ad->image}}</td>
            <td>
                <a href="{{route('ADs.edit',$Ad->id)}}" class="btn btn-primary"><span class="fa fa-edit fa-1x"></span></a>
            </td>
            <td>
                    <form action="{{route('ADs.destroy',$Ad->id)}}" method="post"  enctype="multipart/form-data"  id="delete-form" class="actions-buttons">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit"><span class="fa fa-trash fa-1x delete-btn"></span></button>
                    </form>
            </td>

        </tr>
        @endforeach
        </tbody>
    </table>


@endsection
