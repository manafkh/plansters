@extends('layouts.app')
@section('content')
    <div class="container">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        <form action="{{ route('ADs.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <br/>
                    <input type="text" name="title" class="form-control" placeholder="Add Title">
                </div>
                <div class="col-md-12">
                    <br/>
                    <input type="text" name="target_url" class="form-control" placeholder="Add Target URL">
                </div>
                <div class="col-md-12">
                    <br/>
                    <input type="file" name="image" class="image">
                </div>
                <div class="col-md-12">
                    <br/>
                    <button type="submit" class="btn btn-success">Upload Image</button>
                </div>
            </div>
        </form>
    </div>

    @endsection
